const express = require('express')
const app = express()
const port = 26200

app.get('/', (req, res) => {
  res.send('Hello World!')
  console.log(req.query)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})